//
//  main.m
//  VlcTest
//
//  Created by Pierre Marandon on 20/10/2018.
//  Copyright © 2018 Phylica. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
