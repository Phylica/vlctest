//
//  MainWindowController.h
//  VlcTest
//
//  Created by Pierre Marandon on 21/10/2018.
//  Copyright © 2018 Phylica. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainWindowController : NSWindowController

@end

NS_ASSUME_NONNULL_END
