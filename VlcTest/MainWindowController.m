//
//  MainWindowController.m
//  VlcTest
//
//  Created by Pierre Marandon on 21/10/2018.
//  Copyright © 2018 Phylica. All rights reserved.
//

#import "MainWindowController.h"

@interface MainWindowController ()

@end

@implementation MainWindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
